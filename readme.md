# Technical Challenge
The challenge is to create a program which can record a series of user provided
numbers, then provide some minor analytics.

This is written in python 2.7

### How to Run
To execute this program, run the following:

`python stats.py [ACTION] [FILEPATH] [N1] [N2] .. [Nx]`

`[ACTION]` can be one of the following:

 - 'record': Records [Nx] numbers to [FILEPATH], or
 - 'summary': Summarizes the contents of [FILEPATH]

`[FILEPATH]` is the file you wish to record to and evaluate.

 - Can be in same directory or a run-time defined subfolder of script

`[N1] [N2] .. [Nx]` 

 - any number of arguments when using the [record] function.

#####Example:
```
python stats.py record test/here 12 18 20
python stats.py record test/here 2 8 

python stats.py summary test/here
```

### Method Documentation
Below is a summary of all the methods.

| Method | Purpose | Notes |
| ------ | ------- | ----- |
| `main()` | The main program, calls each relevant step. | Keeping all logic kept in their relevant functions leaves main pretty bare. |
| `validate_args(action=None)` | Ensures the user has provided valid arguments. | The action=None argument allows this function to be re-used while ensuring no needed code is re-executed. To perform validation, this function calls the following: `print_help()`, `validate_action()`, `validate_numbers()`, and `validate_file()`. |
| `get_action()` | Collects and verifies the given action argument. | Alleviates case sensitivity. | 
| `validate_numbers()` | This function ensures the user provided valid numbers to be recorded. | Ensures no entries such as 'two'. |
| `validate_file(readwrite)` | Ensures the user has specified a valid file. | When recording, allows users to create subdirectories. When summarizing, ensures the file exists first. |
| `get_file()` | Handles the file name and relative pathing. | Extracts the information from the user arguments. |
| `get_numbers()` | Simply collects the numeral arguments. | This function is used twice, once in verification and once for actual recording. |
| `take_action(action)` | Calls the appropriate action function. | Checks the validated action, and calls it. |
| `record()` | The logic for recording numbers to a file. | The file name and path are collected from `get_file()` | 
| `summarize()` | The logic for summarizing numbers in a file. | The contents of the file are collected as a list from `get_file_contents()`, whereby they are summarized as per the specification. | 
| `get_file_contents()` | Parses through the file, collecting the content to RAM. | Gets the file name and path from `get_file()`, loops through and returns for summarization. |
| `print_help()` | Prints how to use this program. | This function is called most times the program is incorrectly invoked. |





