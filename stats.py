import sys
import os
import string

def main():
    validate_args()
    action = get_action()
    validate_args(action)
    take_action(action)

def validate_args(action = None):
    if action == None:
        if sys.argv[-1] == "help": print_help()
        if len(sys.argv) < 2: print_help()
        return

    """ When recording, validate the supplied floats and file"""
    if action == "record":
        validate_numbers()
        validate_file('a')

    """Summarizing, ensure valid file"""
    if action == "summary":
        validate_file('r')
        if len(sys.argv) > 3:
            print "\nIgnoring trailing arguments.\n"

def get_action():
    action = str(sys.argv[1]).lower()
    if action != "record" and action != "summary":
        print "Sorry, '" + sys.argv[1] + "' is not a valid command.\n"
        print_help()
    return action

def validate_numbers():
    numbers = get_numbers()
    if not len(numbers):
        print "No values were supplied to record!\n"
        print_help()
    for numeral in numbers:
        try:
            numeral = float(numeral)
        except:
            print "Sorry, '" + numeral + "' is not a valid number!\n"
            print_help()

def validate_file(readwrite):
    filepath, file_name = get_file()
    """allows user to create subdirectories relevative to script path"""
    if filepath and not os.path.exists(filepath):
        os.makedirs(filepath)

    try:
        summary_file = open(file_name, readwrite)
        summary_file.close()
    except:
        print "The specified file does not exist, or is having trouble opening."
        sys.exit()

def get_file():
    script_path = os.path.dirname(__file__)
    full_filepath = sys.argv[2].rsplit('/', 1)

    """File is in a subdirectory of the script."""
    if len(full_filepath) > 1:
        file_path = os.path.join(script_path, full_filepath[0])
        file_name = os.path.join(file_path, full_filepath[1])
        sanitize_filename(file_name)
        return file_path, file_name

    """File is in same directory as script"""
    file_name = sys.argv[2]
    file_path = 0
    sanitize_filename(file_name)
    return file_path, file_name

def sanitize_filename(file_name):
    """ Nice little bit of code from the web :) """
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    file_name = ''.join(char for char in file_name if char in valid_chars)

def get_numbers():
    numbers = []
    for i in range(3, len(sys.argv)):
        numbers.append(sys.argv[i])
    return numbers

def take_action(action):
    if action == "record": record()
    summarize()

def record():
    _, file_name = get_file()

    """ Open file to append """
    summary_file = open(file_name, 'a')

    numbers = get_numbers()
    for each in numbers:
        summary_file.writelines(each + '\n')
    summary_file.close()

    print "\nData recorded.\n"
    sys.exit()

def summarize():
    vals = get_file_contents()
    num_vals = len(vals)
    min_val = float(min(vals))
    max_val = float(max(vals))
    avg_val = sum(vals) / num_vals

    print "\n +--------------+--------+"
    print " | # of Entries | {:<6d} |".format(num_vals)
    print " | Min. value   | {:<6.2f} |".format(min_val)
    print " | Max value    | {:<6.2f} |".format(max_val)
    print " | Avg. value   | {:<6.2f} |".format(avg_val)
    print " +--------------+--------+\n"

def get_file_contents():
    _, file_name = get_file()

    """ Open file for reading """
    summary_file = open(file_name, 'r')
    values = []

    """ Start processing file content """
    line = summary_file.readline()
    while line:
        values.append(float(line))
        line = summary_file.readline()
    summary_file.close()

    return values

def print_help():
    print "This program is invoked by the following:"
    print "\n python stats.py [CMD] [FILEPATH] [N1] .. [Nx]"
    print "\n [CMD] can be either 'record' or 'summary'."
    print " [FILEPATH] is the file you wish to record to or summarize"
    print " [N1] .. [Nx] are the variable number of numerals you wish to record"
    print "\nFor example: python stats.py record test/here 12 13 18.4"
    print "Followed by: python stats.py summary test/here\n"
    sys.exit()

if __name__ == "__main__":
    main()
